# This file is distributed under the same license as the Django package.
# 
# Translators:
# Jaffa McNeill <jaffa.mcneill@gmail.com>, 2012.
#   <kristjan.schmidt@googlemail.com>, 2012.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: django-localflavor-au\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-12-20 16:38-0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Jaffa McNeill <jaffa.mcneill@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"

#: forms.py:26
msgid "Enter a 4 digit postcode."
msgstr "Enigu 4 ciferon postkodon."

#: models.py:9
msgid "Australian State"
msgstr "Aŭstralia ŝtato"

#: models.py:19
msgid "Australian Postcode"
msgstr "Aŭstralia Posta Kodo"

#: models.py:33
msgid "Australian Phone number"
msgstr "Aŭstralia Telefonnumero"
